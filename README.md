# entware-homeautomation
Entware feed for home-automation software. The goal is to add support packages for 
* telldus-core + tellprox
* openHAB
* Home Assistant

Currently the following end-packages are available:
* telldus-core
* tellcore-py
* tellprox
* homeassistant <- coming soon, prerequisites finished

## Target info
Currently compiles (binaries available for download) for the all [Entware](https://github.com/Entware-ng/Entware-ng.git) targets:  
* armv7soft-glibc
* armv5soft-glibc
* x86-64-glibc
* x86-32-glibc
* mipselsf-uClibc

Binaries actually tested for
* armv7soft-glibc


Binaries are available at [Mediafire](https://www.mediafire.com/folder/geyonumlhiyyn/binaries)

## Usage notes
* Clone Entware-ng: https://github.com/Entware-ng/Entware-ng.git
  * Instructions for Entware-ng at: https://github.com/Entware-ng/Entware-ng/wiki/Compile-packages-from-sources 
* Add entware-tellstick to feeds.conf
  * See *install_feed.sh* for example (only run it once if this is preferred)
* Choose platform: 
```
    cp configs/armv7.config .config
```
* Now you have to run the menuconfig to set things up: 
```
    make menuconfig
```
* Build tools and toolchain, if necessary, and packages if you want them
```
    make tools/install
    make toolchain/install
    make target/compile (optional)
    make package/compile (optional)
```
* Build telldus-core as single packe (V=wcs enables all debug output)
```
    make V=wcs package/feeds/homeautomation/telldus-core/compile
```
* Find your freshly build packages in `bin/armv7soft-glibc/packages/*.ipk`
* Copy ipkg-files to router and install using `opkg install filename.ipk`
