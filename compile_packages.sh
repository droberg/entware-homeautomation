#!/bin/bash
# Super dumb compile script
# Looks for already used compile-settings saved in .config.xxx files
# and compiles all packages using all available .config.xxx files


DIR=""
USAGE_STRING="Usage: $(basename $0) [-d 'Entware-ng directory'] ..."
REL_SCRIPT_DIR=$(dirname $0)
ORIGIN_DIR=$(pwd)

while getopts "d:" opt; do
    case "$opt" in
        d)
            DIR=$OPTARG
            ;;
        \?)
            echo  >&2 $USAGE_STRING
            exit 1;;
      esac
done

if [ ! -n "$DIR" ]; then
    echo  >&2 $USAGE_STRING
    exit 1
fi

CONFIGS=$(ls .config.* | grep -v "~" | grep -v "old")
echo "Building for CONFIGS = $CONFIGS"
for CONFIG in $CONFIGS; do
    echo "Building for config: $CONFIG "
    cp $CONFIG .config
    make oldconfig
    cp $CONFIG $CONFIG~
    cp .config $CONFIG
    PACKAGES=$(find $ORIGIN_DIR/$REL_SCRIPT_DIR -name "Makefile" | xargs -n1 dirname | awk -F"/" '{print $(NF)}' )
    for PACKAGE in $PACKAGES; do
        make package/feeds/homeautomation/$PACKAGE/compile
    done
done
