#!/opt/bin/bash
LOG_DIR=/opt/var/log
AWK=/usr/bin/awk
GREP=/bin/grep
# This code assumes that we have busybox ps, possibly things could go wrong if we have  other version
PS=/bin/ps

check() {
    PROGRAMPROCESSES=($($PS -w | $GREP "$PROGRAM_GREP_STR"  2>&1 | $GREP -v $GREP | $GREP -v bash | $AWK '{print $1}'))
    #$PS -w | $GREP "$PROGRAM_GREP_STR"  2>&1 
    if [ -z "$PROGRAMPROCESSES" ];
    then
        echo "No program process found"
        exit 1
    else
        checkalive
    fi
    echo "Found PID: $PROGRAMPROCESSES"
}

start(){
    $PROGRAM_START  &> $LOG_DIR/$PROGRAM_LOG_FILE &
}

stop(){
    PROGRAMPROCESSES=$($PS -w | $GREP "$PROGRAM_GREP_STR"  2>&1 | $GREP -v grep | $GREP -v bash | $AWK '{print $1}')
    for PID in $PROGRAMPROCESSES;
    do
        kill -s 9 $PID
    done
}

runcmd(){
    CMD=$1
    SCRIPT_NAME=`basename "$0"`
    #echo "SCRIPT_NAME $SCRIPT_NAME"
    if [[ $SCRIPT_NAME == *"Check.bash" ]];
    then
	CMD="check"
    fi


    case "$CMD" in
        start)   start ;;
        stop)    stop ;;
        restart) stop; start ;;
        check) check ;;
        *) echo "usage: $0 start|stop|restart|check" >&2
           exit 1
           ;;
    esac
}
