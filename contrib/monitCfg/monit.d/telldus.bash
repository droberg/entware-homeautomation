#!/opt/bin/bash
source ${0%/*}/function_defs.bash

PROGRAM_CMD='/opt/bin/tdtool  --list-devices'
PROGRAM_START="/opt/sbin/telldusd --nodaemon"
PROGRAM_GREP_STR="telldusd"
PROGRAM_LOG_FILE="telldusd.log"

checkalive(){
    RES=$($PROGRAM_CMD 2>&1)
    #echo "RES:  $RES"
    if [[ $RES == *"Could"*"not"*"connect"* ]];
    then
        echo "Connection timed out"
        exit 1
    fi
}

runcmd $1
