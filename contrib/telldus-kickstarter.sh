#!/opt/bin/bash
LOG_DIR=/opt/var/log
AWK=/usr/bin/awk
GREP=/bin/grep
PS=/bin/ps
TELLDUS_CMD="/opt/sbin/telldusd --nodaemon"
TELLDUS_GREP_STR="telldusd"
TELLDUS_LOG_FILE="telldusd.log"
TELLPROX_GREP_STR="tellprox"
TELLPROX_LOG_FILE="tellprox.log"
KICKSTARTER_LOG_FILE="telldus-kickstarter.log"

writeToLog=true
while getopts vp opt
do    case "$opt" in
      v)      writeToLog=false;;
      [?])    echo  >&2 "Usage: $0 [-v] ..."                                                                                                               
              	exit 1;;                                                                                                                                     
      esac
done
if  $writeToLog ; then
  echo "Using log-file: $LOG_DIR/$KICKSTARTER_LOG_FILE"
  exec 1>$LOG_DIR/$KICKSTARTER_LOG_FILE
fi

while true
do
    TELLPROXPROCESSES=$($PS -w | $GREP $TELLPROX_GREP_STR  2>&1 | $GREP -v grep | $AWK '{print $1}')
    TELLDUSPROCESSES=$($PS -w | $GREP $TELLDUS_GREP_STR  2>&1 | $GREP -v grep | $AWK '{print $1}')
    echo "Telldus processes:" $TELLDUSPROCESSES
    if [ -n "$TELLDUSPROCESSES" ];
    then
	DUMMY="";
        echo "Telldus daemon seems to be alive"
    else
        echo "Wierd, no active telldus daemon processes... : $TELLDUSPROCESSES"
	$TELLDUS_CMD  &> $LOG_DIR/$TELLDUS_LOG_FILE &
    fi
    if [ -n "$TELLPROXPROCESSES" ];
    then
	DUMMY="";
        echo "Tellprox seems to be alive"
    else
        echo "Wierd, no active tellprox processes..."
	cd $TELLPROX_DIR
	/opt/bin/python2.7 -m tellprox &> $LOG_DIR/$TELLPROX_LOG_FILE &
    fi
    echo "Looping"
    sleep 5
done

