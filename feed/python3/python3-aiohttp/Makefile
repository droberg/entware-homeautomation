#
# Copyright (C) 2007-2016 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=aiohttp
PKG_VERSION:=1.0.5
PKG_RELEASE:=1
PKG_LICENSE:=Apache 2

PKG_SOURCE:=v$(PKG_VERSION).tar.gz
PKG_SOURCE_URL:=https://github.com/KeepSafe/aiohttp/archive
PKG_MD5SUM:=841e3462235ffa8ad8320face3b34be0
PKG_BUILD_DEPENDS:=python3

include $(INCLUDE_DIR)/package.mk
$(call include_mk, python3-package.mk)

define Package/python3-aiohttp
  SUBMENU:=Python
  SECTION:=lang
  CATEGORY:=Languages
  MAINTAINER:=Per Öberg <per@familjenoberg.se>
  TITLE:=http client/server for asyncio
  URL:=https://github.com/KeepSafe/aiohttp/
  DEPENDS:=+python3 +python3-async-timeout +python3-chardet +python3-multidict +python3-yarl
endef

define Package/python3-aiohttp/description
  Aiohttp:
  * Supports both client and server side of HTTP protocol.
  * Supports both client and server Web-Sockets out-of-the-box.
  * Web-server has middlewares and pluggable routing.
endef

define Build/Compile
	$(call Build/Compile/Py3Mod,,install --prefix=/opt --root=$(PKG_INSTALL_DIR))
endef

define Package/python3-aiohttp/install
	$(INSTALL_DIR) $(1)$(PYTHON3_PKG_DIR)
	$(CP) \
	    $(PKG_INSTALL_DIR)$(PYTHON3_PKG_DIR)/* \
	    $(1)$(PYTHON3_PKG_DIR)
endef

$(eval $(call BuildPackage,python3-aiohttp))
