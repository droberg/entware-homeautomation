#!/bin/bash
FEEDS_CONF_DIR="$1/"
CUSTOM_FEED_DIR="$(pwd)/feed"
echo "src-link homeautomation ${CUSTOM_FEED_DIR}" >> ${FEEDS_CONF_DIR}feeds.conf
echo "Running: ./${FEEDS_CONF_DIR}scripts/feeds update homeautomation"
./${FEEDS_CONF_DIR}scripts/feeds update homeautomation
echo "Running: ./${FEEDS_CONF_DIR}scripts/feeds install -a -p homeautomation"
./${FEEDS_CONF_DIR}scripts/feeds install -a -p homeautomation

