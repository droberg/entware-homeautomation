#!/bin/bash

DIR=""
USAGE_STRING="Usage: $(basename $0) [-d 'Entware-ng directory'] ..."
REL_SCRIPT_DIR=$(dirname $0)
ORIGIN_DIR=$(pwd)

mkdir -p $REL_SCRIPT_DIR/binaries

while getopts "d:" opt; do
    case "$opt" in
        d)
            DIR=$OPTARG
            ;;
        \?)
            echo  >&2 $USAGE_STRING
            exit 1;;
      esac
done

if [ ! -n "$DIR" ]; then
    echo  >&2 $USAGE_STRING
    exit 1
fi

if [ ! -d "$DIR/bin" ]; then
    echo "$DIR/bin does not exists, aborting"
    exit 1
fi


cd $DIR/bin
TARGETS=$(ls)

for TARGET in $TARGETS; do
    mkdir -p $ORIGIN_DIR/$REL_SCRIPT_DIR/binaries/$TARGET
    #echo "BINDIR: $ORIGIN_DIR/$REL_SCRIPT_DIR/binaries/$TARGET"
    #echo "Target: $TARGET"
    PACKAGES=$(find $ORIGIN_DIR/$REL_SCRIPT_DIR -name "Makefile" | xargs -n1 dirname | awk -F"/" '{print $(NF)}' | sed -e 's/_/-/g')
    #echo "PACKAGES = $PACKAGES"
    for PACKAGE in $PACKAGES; do
        #echo "Package: $PACKAGE"
        FILES=$(find . -name "$PACKAGE*"  | grep $TARGET)
        if [ ! -n "$FILES" ]; then
            echo "No binary found for: $TARGET/$PACKAGE"
        fi
        for FILE in $FILES; do
            cp -a $FILE $ORIGIN_DIR/$REL_SCRIPT_DIR/binaries/$TARGET/
        done
        #echo "Files: $FILES"
    done
    
done

cd $ORIGIN_DIR/$REL_SCRIPT_DIR/binaries
for TARGET in $TARGETS; do
    tar jcf $TARGET.tbz2 $TARGET
done
